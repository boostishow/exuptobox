import re
import sys
import pyperclip
import unidecode

from datetime import datetime

from helpers import *


check_quality = ["2160p", "1080p", "720p", "480p"]
check_language = ["MULTi", "FRENCH", "VOSTFR"]


def slugify(text: str) -> str:
    text = unidecode.unidecode(text).lower()
    return re.sub(r'[\W_]+', '-', text)


def get_title(url_infos: str) -> str:
    return_str = str()
    for split in url_infos['file_name'].split('.'):
        if re.search(r'((S|s)[0-9]{2}(E|e)[0-9]{2})', split) is not None:
            return return_str
        if return_str:
            return_str += " "
        return_str += split


def main():
    if len(sys.argv) != 2:
        return print("[Error] Args")

    urls = get_urls(sys.argv[1])
    urls_infos = get_urls_infos(urls)

    data = dict() 
    data_name = get_title(urls_infos[0])

    for url_infos in urls_infos:
        
        url_number = list()
        url_quality_list = list()
        url_source_list = list()

        for split in url_infos['file_name'].split('.'):
            
            (lambda x: url_number.append(x.group(0).upper()) if (x != None) else None)(re.search(r'((S|s)[0-9]{2}(E|e)[0-9]{2})', split))

            (lambda x: url_quality_list.append(x[0]) if (len(x) > 0) else None)(parse_split(split, check_quality))

            (lambda x: url_source_list.append(x[0]) if (len(x) > 0) else None)(parse_split(split, check_language))

        if not url_number:
            print(f"[Error] {url_infos['file_name']}")
            continue

        url_quality = url_quality_list[0]
        url_source = url_source_list[0]
        url_season = url_number[0][1:3]
        url_episode = url_number[0][4:6]

        if not url_season in data:
            data[url_season] = dict()
            [data[url_season].update({ x: dict() }) for x in check_quality]
            [data[url_season][y].update({ x: list() }) for x in check_language for y in data[url_season].keys()]

        data[url_season][url_quality][url_source].append({
            'file_name': url_infos['file_name'],
            'file_code': url_infos['file_code'],
            'file_episode': url_episode
        })

        data[url_season][url_quality][url_source].sort(key=lambda item: item['file_name'])

    return_str = f"""---
title: "{data_name}"
created_at: {datetime.now().strftime("%Y-%m-%d")}
updated_at: {datetime.now().strftime("%Y-%m-%d")}
---\n"""
    for url_season in data.keys():
        return_str += f"\n## Saison {int(url_season)}\n"
        for url_quality in data[url_season].keys():
            for url_source in data[url_season][url_quality].keys():
                urls_list = data[url_season][url_quality][url_source]
                if not urls_list:
                    continue
                return_str += f"\n{url_quality} {url_source}\n"
                for url_list in urls_list:
                    return_str += f"- Episode {int(url_list['file_episode'])}  \n"
                    return_str += f"[https://uptobox.com/{url_list['file_code']}](https://uptobox.com/{url_list['file_code']})  \n"
    return_str += f"<!-- {slugify(data_name)}.md -->"

    pyperclip.copy(return_str)
    print("Clipboard!")


if __name__ == "__main__":
    main()