import requests


def get_files(file_code: str):
    return requests.get(f"https://uptobox.com/api/link/info?fileCodes={file_code}").json()