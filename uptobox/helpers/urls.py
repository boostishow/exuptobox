import re
import sys
import time
import math

from .api import get_files


def get_urls(text: str) -> list:
    regex = r"(?i)\b((?:https?://|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'\".,<>?«»“”‘’]))"
    url = re.findall(regex, text)       
    return [x[0] for x in url]


def get_files_code(urls: list) -> list:
    return [re.match(r"^https:\/\/uptobox\.com\/([a-z0-9]{12})$", url).groups(1)[0] for url in urls]


def concatenate_urls(urls: list) -> str:
    return ",".join(urls)


def cut_list_files_code(files_code: list) -> list:
    return_files_code = list()
    [return_files_code.append(list()) for x in range(math.ceil(len(files_code) / 100))]
    [return_files_code[x].append(y) for x in range(math.ceil(len(files_code) / 100)) for y in files_code[(x * 100):((x + 1) * 100)]]
    return return_files_code


def parse_split(x: str, y: list) -> list:
    return [z for z in y if z.lower() in x.lower()]


# if not re.match(r"^https:\/\/uptobox\.com\/[a-z0-9]{12}$", url):
#     continue
def get_urls_infos(urls: list):
    list_files_code = get_files_code(urls=urls)
    list_cut_files_code = cut_list_files_code(files_code=list_files_code)

    return_data = list()

    for cut_files_code in list_cut_files_code:
        files_codes = concatenate_urls(cut_files_code)
        request = get_files(files_codes)
        if request['statusCode'] != 0:
            print("[Error] Unable to contact Uptobox")
            sys.exit(1)

        [return_data.append({
            'file_code': x['file_code'],
            'file_name': x['file_name']
        }) for x in request['data']['list']]
    
        time.sleep(0.2)

    return return_data
        